We cant run test on nec-wx3000hp until LCM-579 is fixed:

  $ [ "$DUT_BOARD" = "nec-wx3000hp" ] && exit 80
  [1]

Create R alias:

  $ alias R="${CRAM_REMOTE_COMMAND:-}"

Check that ubus has expected datamodels available:

  $ R "ubus list | grep '[[:upper:]]' | grep -v '\.[[:digit:]]'"
  ACLManager
  ACLManager.Role
  Agent
  Bridging
  Bridging.Bridge
  Bridging.Bridge.Port
  Bridging.Bridge.Port.Stats
  Bridging.Bridge.VLAN
  Bridging.Bridge.VLANPort
  Cthulhu
  Cthulhu.Config
  Cthulhu.Container
  Cthulhu.Container.Instances
  Cthulhu.Information
  Cthulhu.Sandbox
  Cthulhu.Sandbox.Instances
  DHCPv4
  DHCPv4.Client
  DHCPv4.Server
  DHCPv4.Server.Pool
  DHCPv6
  DHCPv6.Client
  DHCPv6.Server
  DHCPv6.Server.Pool
  DNS
  DNS.Client
  DNS.Client.Server
  DNS.Relay
  DNS.Relay.Forwarding
  DNSSD
  DNSSD.Service
  Device
  Device.InterfaceStack
  DeviceInfo
  DeviceInfo.DeviceImageFile
  DeviceInfo.FirmwareImage
  DeviceInfo.Location
  DeviceInfo.MemoryStatus
  DeviceInfo.ProcessStatus
  DeviceInfo.ProcessStatus.Process
  DeviceInfo.Processor
  DeviceInfo.VendorConfigFile
  DeviceInfo.VendorLogFile
  Ethernet
  Ethernet.Interface
  Ethernet.Link
  Ethernet.Link.Stats
  Ethernet.VLANTermination
  Ethernet.VLANTermination.Stats
  Firewall
  Firewall.Chain
  Firewall.Level
  Firewall.X_PRPL-COM_DMZ
  Firewall.X_PRPL-COM_Pinhole
  Firewall.X_PRPL-COM_Policy
  Firewall.X_PRPL-COM_PortTrigger
  Firewall.X_PRPL-COM_Service
  IP
  IP.ActivePort
  IP.Interface
  Logical
  Logical.Interface
  ManagementServer
  ManagementServer.ConnRequest
  ManagementServer.InternalSettings
  ManagementServer.State
  ManagementServer.Stats
  ManagementServer.Subscription
  NAT
  NAT.InterfaceSetting
  NAT.PortMapping
  NeighborDiscovery
  NeighborDiscovery.InterfaceSetting
  NetDev
  NetDev.ConversionTable
  NetDev.ConversionTable.Protocol
  NetDev.ConversionTable.Scope
  NetDev.ConversionTable.Table
  NetDev.Link
  NetDev.Stats
  NetModel
  NetModel.Intf
  PPP
  PPP.Interface
  PPP.Interface.IPCP
  PPP.Interface.IPv6CP
  PPP.Interface.PPPoE
  PPP.Interface.Stats
  QoS
  QoS.Classification
  QoS.Node
  QoS.Queue
  QoS.QueueStats
  QoS.Scheduler
  QoS.Shaper
  Rlyeh
  Rlyeh.Images
  RouterAdvertisement
  RouterAdvertisement.InterfaceSetting
  RouterAdvertisement.InterfaceSetting.Option
  Routing
  Routing.RIP
  Routing.RIP.InterfaceSetting
  Routing.RouteInformation
  Routing.RouteInformation.InterfaceSetting
  Routing.RouteInformation.InterfaceSetting.X_PRPL-COM_Option
  Routing.Router
  Routing.Router.IPv6Forwarding
  SSH
  SSH.Server
  SoftwareModules
  SoftwareModules.DeploymentUnit
  SoftwareModules.ExecEnv
  SoftwareModules.ExecutionUnit
  Time
  Time.X_PRPL-COM_TimeServer
  Time.X_PRPL-COM_TimeServer.Intf
  Timingila
  UPnP
  UPnP.Device
  UserInterface
  UserInterface.HTTPAccess
  UserInterface.HTTPAccess.X_PRPL-COM_HTTPConfig
  UserInterface.HTTPProxy
  UserInterface.HTTPVHost
  Users
  Users.Group
  Users.Role
  Users.SupportedShell
  Users.User
  WiFi
  WiFi.AccessPoint
  WiFi.AccessPoint.AssociatedDevice
  WiFi.AccessPoint.AssociatedDevice.ProbeReqCaps
  WiFi.AccessPoint.AssociationCount
  WiFi.AccessPoint.AssociationCount.FastReconnectTypes
  WiFi.AccessPoint.DriverConfig
  WiFi.AccessPoint.HotSpot2
  WiFi.AccessPoint.IEEE80211r
  WiFi.AccessPoint.IEEE80211u
  WiFi.AccessPoint.MACFiltering
  WiFi.AccessPoint.MACFiltering.Entry
  WiFi.AccessPoint.MACFiltering.TempEntry
  WiFi.AccessPoint.Neighbour
  WiFi.AccessPoint.ProbeFiltering
  WiFi.AccessPoint.ProbeFiltering.TempEntry
  WiFi.AccessPoint.RssiEventing
  WiFi.AccessPoint.Security
  WiFi.AccessPoint.VendorIEs
  WiFi.AccessPoint.VendorIEs.VendorIE
  WiFi.AccessPoint.WPS
  WiFi.AutoCommitMgr
  WiFi.EndPoint
  WiFi.EndPoint.AssocStats
  WiFi.EndPoint.Profile
  WiFi.EndPoint.Profile.Security
  WiFi.EndPoint.Security
  WiFi.EndPoint.Stats
  WiFi.EndPoint.WPS
  WiFi.Radio
  WiFi.Radio.ChannelMgt
  WiFi.Radio.DFS
  WiFi.Radio.DFS.Event
  WiFi.Radio.DriverConfig
  WiFi.Radio.DriverStatus
  WiFi.Radio.EventCounter
  WiFi.Radio.MACConfig
  WiFi.Radio.NaStaMonitor
  WiFi.Radio.NaStaMonitor.MonitorDevice
  WiFi.Radio.NaStaMonitor.NonAssociatedDevice
  WiFi.Radio.NaStaMonitor.RssiEventing
  WiFi.Radio.RadCaps
  WiFi.Radio.ScanConfig
  WiFi.Radio.ScanResults
  WiFi.Radio.ScanResults.SurroundingChannels
  WiFi.Radio.ScanResults.SurroundingChannels.Accesspoint
  WiFi.Radio.ScanResults.SurroundingChannels.Accesspoint.SSID
  WiFi.Radio.ScanStats
  WiFi.Radio.ScanStats.ScanReason
  WiFi.Radio.Stats
  WiFi.Radio.Stats.WmmBytesReceived
  WiFi.Radio.Stats.WmmBytesSent
  WiFi.Radio.Stats.WmmFailedBytesReceived
  WiFi.Radio.Stats.WmmFailedReceived
  WiFi.Radio.Stats.WmmFailedSent
  WiFi.Radio.Stats.WmmFailedbytesSent
  WiFi.Radio.Stats.WmmPacketsReceived
  WiFi.Radio.Stats.WmmPacketsSent
  WiFi.Radio.Vendor
  WiFi.SSID
  WiFi.SSID.Stats
  WiFi.SSID.Stats.WmmBytesReceived
  WiFi.SSID.Stats.WmmBytesSent
  WiFi.SSID.Stats.WmmFailedBytesReceived
  WiFi.SSID.Stats.WmmFailedReceived
  WiFi.SSID.Stats.WmmFailedSent
  WiFi.SSID.Stats.WmmFailedbytesSent
  WiFi.SSID.Stats.WmmPacketsReceived
  WiFi.SSID.Stats.WmmPacketsSent
  WiFi.wps_DefParam
  XPON
  XPON.ONU
  X_PRPL-COM_MultiSettings
  X_PRPL-COM_MultiSettings.Profile
  X_PRPL-COM_PersistentConfiguration
  X_PRPL-COM_PersistentConfiguration.Config
  X_PRPL-COM_PersistentConfiguration.Config.Security
  X_PRPL-COM_PersistentConfiguration.Service
  X_PRPL-COM_WANAutoSensing
  X_PRPL-COM_WANAutoSensing.Config
  X_PRPL-COM_WANAutoSensing.Detect
  X_PRPL-COM_WANManager
  X_PRPL-COM_WANManager.WAN
